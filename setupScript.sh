#!/bin/sh
set -e

cwd="$(pwd)"

if cd exam-accountmanager; then git pull; else git clone https://gitlab.com/02267-group-12/exam/exam-accountmanager.git exam-accountmanager; fi

cd "$cwd"

if cd exam-dtupay; then git pull; else git clone https://gitlab.com/02267-group-12/exam/exam-dtupay.git exam-dtupay; fi

cd "$cwd"

if cd exam-payment; then git pull; else git clone https://gitlab.com/02267-group-12/exam/exam-payment.git exam-payment; fi

cd "$cwd"

if cd exam-systemtests; then git pull; else git clone https://gitlab.com/02267-group-12/exam/exam-systemtests.git exam-systemtests; fi

cd "$cwd"

if cd exam-token-manager; then git pull; else git clone https://gitlab.com/02267-group-12/exam/exam-token-manager.git exam-token-manager; fi

cd "$cwd"

if cd exam-reporting; then git pull; else git clone https://gitlab.com/02267-group-12/exam/exam-report.git exam-reporting; fi

cd "$cwd/exam-dtupay"
mvn package
sudo docker-compose build

cd "$cwd/exam-reporting"
mvn package
sudo docker-compose build

cd "$cwd/exam-accountmanager"
mvn package
sudo docker-compose build

cd "$cwd/exam-payment"
mvn package
sudo docker-compose build

cd "$cwd/exam-token-manager"
mvn package
sudo docker-compose build

#cd "$cwd/exam-systemtests"
#mvn package
#sudo docker-compose build

cd "$cwd/exam-dtupay"
chmod +x ./build_and_run.sh
sh ./build_and_run.sh